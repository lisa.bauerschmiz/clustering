import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
from pylab import *
import pandas as pd

def column(matrix, i):
    return [row[i] for row in matrix]
    
# Compute clustering with MeanShift
def ms_clustering(x, b):

    # The following bandwidth can be automatically detected using
    # bandwidth = estimate_bandwidth(np.asarray(x), quantile=0.2)
    ms = MeanShift(bandwidth=b, bin_seeding=True)
    y_pred = ms.fit_predict(x)

    return(y_pred)

def prepare_data(new_x, b):
    x_column = np.expand_dims(column(new_x, 0),axis=1)
    clusters = ms_clustering(x_column,b)
    return(clusters)

def cluster_together(y1,y2): 
    max_y1 = max(y1)
    new_y2cluster_IDs = []
    for y1s, y2s in zip(y1,y2):
        new_y2cluster_IDs.append(y2s + max_y1 + 1)
    new_y2 = np.asarray(new_y2cluster_IDs)
    n_clusters = (max(new_y2))+1

    cluster_together = []
    cluster_ID_list = []

    i = 0
    for y1s, y2s in zip(y1,new_y2):

        if [y1s,y2s] not in cluster_ID_list:
            cluster_together.append(i)
            cluster_ID_list.append([y1s,y2s])
            i += 1

        else:
            cluster_together.append(cluster_ID_list.index([y1s,y2s]))
    return(cluster_together)

def create_vectors(x, vertical_clusters):
    # create pandas dataframe and append cluster data of vertical clusters
    labels = ['tick','x','y','state']
    data = pd.DataFrame.from_records(x, columns=labels)
    data['cluster_ID'] = pd.Series(vertical_clusters, index=data.index)
    data = data.sort_values(by='tick')
    #data

    # extract push data and drop duplicates according to segment value
    #push_data = data.loc[data['state'] == 1]
    cl_data = data.groupby(["cluster_ID"])
    unique_data = []
    for key, item in cl_data:
        unique_data.append(cl_data.get_group(key).drop_duplicates("y"))
    unique_df = pd.concat(unique_data)

    # sort the data according to the segment value in each cluster
    y_dataframe = unique_df.groupby("cluster_ID")
    sorted_list = []
    for key, item in y_dataframe:
        sorted_list.append(y_dataframe.get_group(key).sort_values(by="y"))
    sorted_data = pd.concat(sorted_list)

    # extract the cluster order
    a = data.iloc[:,4].values
    cluster_order = [a[i] for i in sorted(np.unique(a, return_index=True)[1])]
    vectors = []
    times = []
    for item in cluster_order:
        vectors.append(sorted_data[sorted_data["cluster_ID"] == item]["y"].values)
        times.append(sorted_data[sorted_data["cluster_ID"] == item]["tick"].values)
    return(vectors, times, data)

# Cut off stripes into 2 seperate ones (according to the two entries/exits)
def seperate_stripes(x):
    x_top = []
    x_bottom = []
    for item in x:
        if item[2] > 10:
            x_top.append(item)
        else:
            x_bottom.append(item)
    return(x_top, x_bottom)

def cluster_and_plot(b, x):
    vertical_clusters = prepare_data(x, b)
    plot_sensor_data(x, vertical_clusters)
    return(vertical_clusters)

# Plot the sensor data (segment against time) according to push/release and sensor stripe
def plot_sensor_data(x_data, y1):
    y1_a = np.unique(np.asarray(y1))
    n = len(y1_a)
    colors = ['b','g','maroon','blueviolet','seagreen','indigo','grey',
            'orange','lightgreen','magenta','purple','plum',
            'lime','cyan','pink','chocolate','orchid','y', 'black', 'navy', 'brown', 'lavender']
    if n> len(colors):
        print('Cannot display more than '+str(len(colors))+' clusters.')
    else:

        color = colors[:n]
        fig = plt.figure(figsize=(25,15))
        ax = fig.add_subplot(111) 

        #subplots
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212, sharex=ax1, sharey=ax1)

        #plot according to push/release and which sensor
        for x, y, index in zip(x_data, y1, list(range(0, len(x_data)))):
            chosen_color = color[y]
            if x[1] == 0:
                if x[3] == 0:
                    ax1.plot(x[0], x[2], marker='o', color = chosen_color, fillstyle = 'none')
                else:
                    ax1.plot(x[0], x[2], marker = 'o', color = chosen_color)
            else: 
                if x[3] == 0:
                    ax2.plot(x[0], x[2], marker='o', color = chosen_color, fillstyle = 'none')
                else:
                    ax2.plot(x[0], x[2], marker = 'o', color = chosen_color)

        # turn off spines and labels of overlayed plot (for the shared axes)
        ax.spines['top'].set_color('none')
        ax.spines['bottom'].set_color('none')
        ax.spines['left'].set_color('none')
        ax.spines['right'].set_color('none')
        ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
        ax1.tick_params(labelcolor='w', top='off', bottom='off', right='off', labelbottom = 'off',  labelsize = 16)
        ax1.tick_params(labelcolor='black', left = 'on')
        ax1.set_ylim([-1, 20])
        ax2.set_ylim([-1, 20])        
        #ax1.set_xlim([100, 200])
        #ax2.set_xlim([100, 200])
        ax2.tick_params(labelsize = 16)
        # Set common labels
        ax.set_xlabel('time[ms]', size = 16, labelpad=20)
        ax.set_ylabel('segment[1/3m]', size = 16, labelpad=20)

        #plt.suptitle('Prediction: '+str(wrong_y_pred)+' True value: '+str(y_true)+ ('\n0= Car in, 1= Car out, 2= Motorcycle, 3= Pedestrian'), va = 'bottom')
        plt.show()

def pairwise_cluster(vectors):
    labels = []
    i = 0
    for a in vectors:
        if i % 2 == 0:
            if i != len(vectors)-1: # don't compare last one
                a = vectors[i]
                b = vectors[i+1]
                value = compare_similarity(a,b)
                if value == True:
                    labels.append([i, i +1])


        i += 1

    return(labels)

def compare_similarity(a,b):
    if a.size != b.size:
        if a.size > b.size:
            c = np.in1d(a,b)
        elif b.size > a.size:
            c = np.in1d(b,a)
    else: 
        c = np.in1d(a,b)
    if np.sum(c)/c.size < 0.5: # depending on threshold
        booleans = []
        for a_value, c_value in zip(a, c):
            if c_value == False:
                for v in b:

                    # numpy allow small differences in y 
                    boolean = np.isclose(a_value, v, atol = 1.4) # or rtol 1?
                    booleans.append(boolean)
                        
        if booleans != []:
            if np.sum(booleans)/a.size >= 0.5:
                value = True
            else:
                value = False
        else: 
            print("Empty input!", booleans)
            
    elif np.sum(c)/c.size >= 0.5:
        value = True

    else: 
        value = False
    return(value)

def cluster_data(b, x):
    clusters = cluster_and_plot(b, x)
    vectors, times, cl_data = create_vectors(x, clusters)
    labels = pairwise_cluster(vectors)
    input_data = get_clusters(cl_data, labels)
    return(input_data)

def get_clusters(cl_data, labels):
    print('Found '+ str(len(labels))+ ' clusters.')
    clusters = []
    cl_data.loc[:, "clusterid"] = -1 # clusterid is the ID of the merged vertical cluster IDs
    cluster_index = 0
    for cluster in labels:
        uni_IDs = pd.Series(cl_data["cluster_ID"]).unique()
        i = (uni_IDs[cluster[0]])
        j = (uni_IDs[cluster[1]])
        i_indices = cl_data["cluster_ID"] == i
        j_indices = cl_data["cluster_ID"] == j
        cl_data.loc[i_indices,'clusterid'] = cluster_index
        cl_data.loc[j_indices,'clusterid'] = cluster_index
        cluster_index += 1
        cl_part_1 = cl_data[cl_data["cluster_ID"] == i].loc[:,["tick", "x", "y", "state"]].values.tolist()
        cl_part_2 = cl_data[cl_data["cluster_ID"] == j].loc[:,["tick", "x", "y", "state"]].values.tolist()

        clus = cl_part_1 + cl_part_2
        clusters.append(np.asarray(clus))

    return(np.asarray(clusters))

def plot_merged_clusters(x_array):
    x_data = []
    labels = []
    i = 0
    for cluster in x_array:
        cluster_list = cluster.tolist()
        x_data = x_data + cluster_list
        labels = labels + [i]*len(cluster)

        i += 1

    n = len(x_array)
    colors = ['b','g','maroon','blueviolet','seagreen','indigo','grey',
            'orange','lightgreen','magenta','purple','plum',
            'lime','cyan','pink','chocolate','orchid','y', 'black', 'navy', 'brown', 'lavender']
    if n > len(colors):
        print('Cannot display more than '+str(len(colors))+' clusters.')
    else:

        color = colors[:n]
        fig = plt.figure(figsize=(25,15))
        ax = fig.add_subplot(111)

        #subplots
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212, sharex=ax1, sharey=ax1)

        #plot according to push/release and which sensor
        for x, index, y in zip(x_data, list(range(0, len(x_data))), labels):
            chosen_color = color[y]

            if x[1] == 0:
                if x[3] == 0:
                    ax1.plot(x[0], x[2], marker='o', color = chosen_color, fillstyle = 'none')
                else:
                    ax1.plot(x[0], x[2], marker = 'o', color = chosen_color)
            else: 
                if x[3] == 0:
                    ax2.plot(x[0], x[2], marker='o', color = chosen_color, fillstyle = 'none')
                else:
                    ax2.plot(x[0], x[2], marker = 'o', color = chosen_color)

        # turn off spines and labels of overlayed plot (for the shared axes)
        ax.spines['top'].set_color('none')
        ax.spines['bottom'].set_color('none')
        ax.spines['left'].set_color('none')
        ax.spines['right'].set_color('none')
        ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
        ax1.tick_params(labelcolor='w', top='off', bottom='off', right='off', labelbottom = 'off',  labelsize = 16)
        ax1.tick_params(labelcolor='black', left = 'on')
        ax1.set_ylim([-1, 20])
        ax2.set_ylim([-1, 20])        
        #ax1.set_xlim([100, 200])
        #ax2.set_xlim([100, 200])
        ax2.tick_params(labelsize = 16)
        # Set common labels
        ax.set_xlabel('time[ms]', size = 16, labelpad=20)
        ax.set_ylabel('segment[1/3m]', size = 16, labelpad=20)

        #plt.suptitle('Prediction: '+str(wrong_y_pred)+' True value: '+str(y_true)+ ('\n0= Car in, 1= Car out, 2= Motorcycle, 3= Pedestrian'), va = 'bottom')
        plt.show()

def binary_to_string(layer_output):
    string_output = []
    for entry in layer_output:
        if entry.tolist() == [1, 0, 0, 0]:
            string_output.append("CAR_IN")
        elif entry.tolist() == [0, 1, 0, 0]:
            string_output.append("CAR_OUT")          
        elif entry.tolist() == [0, 0, 1, 0]:
            string_output.append("PEDESTRIAN")          
        elif entry.tolist() == [0, 0, 0, 1]:
            string_output.append("MOTORCYCLE")           
    return(string_output)